package uk.co.waracle.alarmtest;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String ACTION_PETE = "uk.co.waracle.alarmtest.ACTION_PETE";
    private static final int REQUEST_ALARM = 1;
    private BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.sendIntentDirectly)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendBroadcast(new Intent(ACTION_PETE));
                    }
                });

        findViewById(R.id.schedule).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scheduleCalculation();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(ACTION_PETE);
        registerReceiver(receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(context, "Hello", Toast.LENGTH_SHORT).show();
            }
        }, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    private PendingIntent getLifeSpaceAlarmPendingIntent() {
        return PendingIntent.getBroadcast(
                this,
                REQUEST_ALARM,
                new Intent(ACTION_PETE), // OR   new Intent(this, MyAlarmReceiver.class)
                PendingIntent.FLAG_CANCEL_CURRENT
        );
    }


    private void scheduleCalculation() {
        long alarmTime = System.currentTimeMillis() + 3 *  1000;

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                alarmTime,
                getLifeSpaceAlarmPendingIntent()
        );
    }


    public static class MyAlarmReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("LifeSpaceService", "Broadcast received");
            context.sendBroadcast(new Intent(ACTION_PETE));
        }
    }
}
